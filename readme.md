# Overview

This project provides guidance and tooling to achieve adherence to the Maplecroft coding standards.  

All software developed at Maplecroft must adhere to these standards and the practices outlined in Maplecroft's SDLC.  This is to ensure Maplecroft code is always secure, readable and maintainable.

Please note that this repository is made **public** for ease of reference and use across the Maplecroft business.

[TOC]


## Background

There are a wide range of applications and services at Maplecroft developed by different teams.

To effectively maintain our applications and allow for our code to be usefully shared across groups, Maplecroft require coding standards to be followed by those developing software. 

To support maintaining the approved standards, Maplecroft have adopted tooling including Nitpick, tslint, mypy and black.  These tools help ensure code is secure and appropriately formatted, both during development and prior to commmitting changes.  

Use of these tools consistently across the business can be a challenge since their associated configurations can be subjective, therefore, Maplecroft provide configuration files for these tools to avoid ambiguity in how they are setup and used. 

This repository provides configurations that define:

1. The static code analysis that Maplecroft expects to be run on our code
2. Configurations for these static analysers
3. Reusable CI/CD templates that assist in compliance to the above

The sections below describe how to use these standards within your own code or repository.


## Python Standards

### Enforced Standards
The tooling and configuration provided by this repository checks for and helps preserve the following for Python:

- Enforces adherance to the PEP8 coding styles for Python - https://peps.python.org/pep-0008/
- Specifies a maximum line length of 120 characters
- Uses a generic and automatic code formatter to correct common issues detected by the above
- Checks for common security vulnerabilities in how we code
- Checks for how Python type hints have been used and whether they have been used consistently
- Triggers these checks and actions based on git pre-commit hooks (if correctly configured)


### Tools used
The following tools are to be used to help maintain Python coding standards adherance:

- **[Bandit](https://bandit.readthedocs.io/en/latest/)** - Identifies common security issues in coding approaches to help prevent possible vulnerabilities
- **[Black](https://black.readthedocs.io/en/stable/)** - Auto-formats code in a consistent manner
- **[Mypy](https://mypy.readthedocs.io/en/stable/)** - Provides a means of checking [Python type hints (PEP 484)](https://peps.python.org/pep-0484/), enforcing that there are not mismatches in assigned type definitions and presumed use
- **[Nitpick](https://nitpick.readthedocs.io/en/latest/)** - Provides a means of checking (and auto-fixing) code against [PEP 8](https://peps.python.org/pep-0008/) using [Flake8](https://flake8.pycqa.org/en/latest/).  Nitpick offers multi-langage support, allowing it to be used across multiple projects regardless of coding language utilised. 
- **[Pre-commmit](https://pre-commit.com/)** - Is used to tie in to local git hooks and ensure the above tools are utilised before being committed to a git repository


## Quick start guide to setting up your Python project
To add the required tooling to your project, there are some pre-requisites to first review:

- You must be working within a bash (or similar) terminal with `curl`, `git` and `pip` installed and available
- Your `pip` and Python installation is in a project specific environment, and that you're using a supported Python version.  See the [Python Packaging User Guide](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/) for more information on using virtual environments.  You *must not* use your global Python environment.
- You are working within a project already added to Git with sufficient permissions to write files to it.  Please speak to the Technology team if you require assistance setting up a new Git project.
- The files referenced below do not already exist inside your project folder.  If they do, please **do not** run these steps - please review the files within this repository and merge the required changes into your project configuration files.

With the pre-requisites confirmed, please follow these setup steps:

```
# Install package dependencies
pip install black bandit flake8 flake8-bugbear nitpick pre-commit

# Create the pyproject.toml file
curl -O https://bitbucket.org/maplecroft/standards/raw/master/pyproject.toml 

# Set up pre-commit hooks
curl -O https://bitbucket.org/maplecroft/standards/raw/master/.pre-commit-config.yaml
pre-commit install

# Download flake8 configuration 
curl -O https://bitbucket.org/maplecroft/standards/raw/master/setup.cfg

# Download bandit configuration
curl -O https://bitbucket.org/maplecroft/standards/raw/master/.bandit.yaml

# Add these files to git
git add pyproject.toml .pre-commit-config.yaml setup.cfg .bandit.yml
git commit --no-verify -m "feat: Adding Maplecroft standards tools and configuration to this repository per the README" setup.cfg .pre-commit-config.yaml pyproject.toml .bandit.yml
```

You can then check your setup by running:

    nitpick check


For any reported issues by nitpick, you can also attempt to automatically remediate them by running the below command.  Please note this command
will modify your files in order to correct issues arising:

    nitpick fix


Additionally, you will now find commands for `black`, `bandit`, `mypy`, `flake8` and `pre-commit` are functional within your repository.  Please refer to their online documentation for an introduction to the individual tools and the options you can use with them.

If issues in any steps have arisen, please speak to the Technology Team for assistance.


### What just happened?
The steps above install the current set of tooling we use to manage Python code in our projects, installed their required configuration files and ensured they are used whenever you are committing files to the project.  The files added have the following purposes:

- **pyproject.toml** - Specifies configuration for Black, MyPy and Nitpick across the project.  Nitpick configuration links directly back to the style.toml contained within this Maplecroft standards repository.
- **.pre-commit-config.yaml** - Specifies what tools to run before a `git commit` is completed.  This ensures the tools installed are utilised on the code before it is pushed into the repository
- **setup.cfg** - Specifies flake8 configuration for Python linting checks
- **.bandit.yml** - Sets up Bandit static code analysis for security issue reporting

These files have been added to your project.  As every project differs in terms of requirements and layout, there may be some need to
update these configuration files to better align with your project needs.  


### Help, I can't commit to git!
Congratulations, it seems our compliance checks are working and you have some issues in your project that need addressing.  Please refer
to the output of the git commit attempt to better understand what those issues are and how to correct them.  Most issue reports are shown
with an error code which is easily searchable online with steps to remediate.  If you are struggling to interprate these messages,
please ask the Technology Team.


If you can't get past these messages or are not quite yet ready to ensure your code is fully compliant, you can use the `--no-verify` flag
to the git commit command:

    git commit --no-verify

Please also review the [Further configuration](#markdown-header-further-configuration) options available for adding, customising or (if necessary) disabling some pre-commit checks.

 
### What's next?
If you are intending to use [automated pipelines](#markdown-header-bitbucket) or testing as part of your project, please consider updating your own `requirements.txt` files used in these pipelines to include the Python dependencies installed by this project.


Please also update or create a README.md file for your project clearly explaining the use of the Maplecroft standards repository and linking to it.   Please also make it explicit that the pre-commit installation should be performed
within any copies of the repository.

The snippet below is provided for easy of copy pasting.

```
# Coding Standards
This project uses the [maplecroft standards](https://bitbucket.org/maplecroft/standards/) templated tooling for applicable code
and security standards.  Please refer to the project documentation for use of compliance tooling.

Please ensure you have ran the below commands in the project directory:

    pip install pre-commit
    pre-commit install

```

## Further configuration

### Disclaimer
The configuration files added to your project are copies made for customisation as required by the project you are working on.  However, please limit configuration changes only to those which are beneficial for the overall level of
standards and compliance for the project.

You should not have to disable or ignore security, compliance and standards checking in order to make progress on your project.  Such actions should be seen as a last resort and a choice that acrues technical debt.

You may, however, need to tailor how checks are done, on which files they are done or update specific configuration to
ensure ease of security, standards and compliance adherance.  Please see below for some general guidance on how to
configure your project in detail.


### Nitpick
If using the quick-start guide here, `nitpick` has been set up to provide the minimum standards set by Maplecroft. Any additional per project options can be added to the relevant config files, be that [Pre-commit hooks](#markdown-header-pre-commit-hooks), [Bandit](#markdown-header-bandit), [flake8](#markdown-header-flake8) or [MyPy](#markdown-header-mypy)


### Pre-commit hooks
Changes to the pre-commit hooks ran by git can be done by modifying the `.pre-commit-config.yaml` file within
your repository.  The checks performed can be modified to enable features of the pre-commit libraries referenced in this file, such as adding additional arguments or parameters.  Please refer to the [pre-commit](https://pre-commit.com/#creating-new-hooks) documentation for a detailed breakdown of what is possible.

### Flake8
Configuration changes to flake8 can be made by modifying the `setup.cfg` file.  Please refer to [flake8](https://flake8.pycqa.org/en/latest/user/configuration.html) documentation for more information.  However, please note that flake8 configuration change should be discussed with the Technology Team as possible additions to this project.


### MyPy
To specify additional my-py dependencies on specific files, you can modify the `.pre-commit-config.yaml` file:

```toml
  - repo: https://github.com/pre-commit/mirrors-mypy
    rev: v0.910
    hooks:
      - id: mypy
        files: datax_decoders
        # additional per project config
        additional_dependencies: ["types-python-dateutil", "types-PyYAML"]
```

### Bandit
Bandit usage is complicated https://github.com/PyCQA/bandit/issues/606, it doesn't support pyproject.toml, and it
doesn't auto load configurations. Additionally, it's not natively supported by nitpick which make it difficult
for this project to manage its configuration. Therefore, its suggested that each project manage its own bandit
configuration. We expect all test errors to be enabled and the configuration to maintain options like include/exclude directories.
The default pre-commit hook expects a `.bandit.yaml` configuration file available at the current directory.


## Git commit messages
By default this project does not include any constraints around Git commit messages structure.  However, it would
be considered best practice to enforce good commit messages within your repository in order to ensure the history
of the project is well recorded.

This can be done through use of the [conventional-pre-commit](https://github.com/compilerla/conventional-pre-commit) plugin
for pre-commit which enforces the use of [Conventional Commit Messages](https://www.conventionalcommits.org/) within your project.

To do so, you can extend the `.pre-commit-config.yaml` file to include the following section:

```
  - repo: https://github.com/compilerla/conventional-pre-commit
    rev: v3.2.0
    hooks:
      - id: conventional-pre-commit
        stages: [commit-msg]
```

Once done, ensure the commit-msg hook is enabled using `pre-commit` command:

    pre-commit install --hook-type commit-msg



##  Other languages
At present, this repository only supports and provides tooling for Python coding standards.  Please refer to our
LINK developer documentation for more general guidance.

In future, we will publish update this repository to cover TypeScript.


## Additional Tooling

### IDE

If you are using an IDE for development (and you should) then it is recommended to use the supported features
during development to assist in compliance to the maplecroft standards.

Setup links are provided for two of the most popular IDEs PyCharm and VSCode below.

https://www.jetbrains.com/help/pycharm/configuring-third-party-tools.html

https://code.visualstudio.com/docs/python/linting

You will find at the root of this project a `requirements.txt` that defines the versions of the tools we use.


### BitBucket
The repo comes with a bitbucket-pipelines.yaml file that is included to show how a BitBucket repository can be updated
to run pipelines ensuring code in that repository adheres to maplecroft standards. Please reference this file or use it as
the basis for your own bitbucket-pipelines.yml file when creating new code repositories.

