from setuptools import setup, find_packages

def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]

setup(
    name="maplecroft_standards",
    version="1.2.0",
    description="Maplecroft standards configuration for Python",
    url="https://bitbucket.org/maplecroft/standards/",
    author="Verisk Maplecroft",
    author_email="swdev@maplecroft.com",
    license="MIT",
    packages=find_packages(),
    package_data={"maplecroft_standards": ["*.toml"]},
    install_requires=parse_requirements("requirements.txt"),
    extras_require={},
    python_requires=">=3.7, <4",
)
