# Changelog

<!--next-version-placeholder-->

## v1.2.0 (2024-07-18)
### Feature
* Ignore schedules ([`d02213d`](https://bitbucket.org/maplecroft/standards/commit/d02213de8cef5764dbc9aee8c1fd8557047f540f))
* Bump all versions ([`5bb525e`](https://bitbucket.org/maplecroft/standards/commit/5bb525ed28a0fa28ca4254b9155b5a2f398ac17b))

### Fix
* Add missing BB reference for semantic versioning ([`955a705`](https://bitbucket.org/maplecroft/standards/commit/955a7052aa928111e8f7c6902ff1122584c24ca7))
* **DX-5122:** Mypy version ([`a0e22d5`](https://bitbucket.org/maplecroft/standards/commit/a0e22d5af74598ff02f4bca9cb3e9254a61a661f))
* Add --strip-extras to pip-compile so it can be used as constraint ([`2e70a86`](https://bitbucket.org/maplecroft/standards/commit/2e70a8689496f9da78c67440d27e051c1ec4f4b0))
* Precommit black ([`f7afe73`](https://bitbucket.org/maplecroft/standards/commit/f7afe738dfd2dfab23e12f25d9b7f6db6313d87d))
* Black fails ([`f1739a5`](https://bitbucket.org/maplecroft/standards/commit/f1739a5594fdac69ca8ed51597f002d5c0d413bb))
* Typos and style. ([`8cc9719`](https://bitbucket.org/maplecroft/standards/commit/8cc9719f9138f920f11e6b477620f8035baf91f3))

## v1.1.0 (2024-07-18)
### Feature
* Ignore schedules ([`d02213d`](https://bitbucket.org/maplecroft/standards/commit/d02213de8cef5764dbc9aee8c1fd8557047f540f))
* Bump all versions ([`5bb525e`](https://bitbucket.org/maplecroft/standards/commit/5bb525ed28a0fa28ca4254b9155b5a2f398ac17b))

### Fix
* Add missing BB reference for semantic versioning ([`955a705`](https://bitbucket.org/maplecroft/standards/commit/955a7052aa928111e8f7c6902ff1122584c24ca7))
* **DX-5122:** Mypy version ([`a0e22d5`](https://bitbucket.org/maplecroft/standards/commit/a0e22d5af74598ff02f4bca9cb3e9254a61a661f))
* Add --strip-extras to pip-compile so it can be used as constraint ([`2e70a86`](https://bitbucket.org/maplecroft/standards/commit/2e70a8689496f9da78c67440d27e051c1ec4f4b0))
* Precommit black ([`f7afe73`](https://bitbucket.org/maplecroft/standards/commit/f7afe738dfd2dfab23e12f25d9b7f6db6313d87d))
* Black fails ([`f1739a5`](https://bitbucket.org/maplecroft/standards/commit/f1739a5594fdac69ca8ed51597f002d5c0d413bb))
* Typos and style. ([`8cc9719`](https://bitbucket.org/maplecroft/standards/commit/8cc9719f9138f920f11e6b477620f8035baf91f3))
